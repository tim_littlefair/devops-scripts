#!/bin/sh

packer=~/bin/packer
timestamp=`date +%Y%m%d%H%M%S`

export PACKER_CACHE_DIR=../../cache

$packer build -var "ts=$timestamp" local.template.json 
