#!/bin/sh

# packer=~/bin/packer
# export PACKER_CACHE_DIR=../../cache

timestamp=`date +%Y%m%d%H%M%S`
outdir=../../output/installer_dmg_$timestamp
mkdir -p $outdir
sudo sh ./prepare_iso.sh ../../input/macOS-Sierra-installESD.dmg $outdir -D DISABLE_REMOTE_MANAGEMENT -D DISABLE_SCREEN_SHARING | tee $outdir/build.log 2>&1 
