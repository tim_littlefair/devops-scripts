#!/bin/sh

sudo apt-get update

# Put a useable terminal and text editor on
# TODO: Move this to a different script
sudo apt-get -y install roxterm mousepad

# Install the Ubuntu Java 8
sudo apt-get -y install openjdk-8-jdk-headless

cd /opt

# Install Oracle Java 8 as an alternative to openjdk
JDK_VERSION=jdk-8u131-linux-x64
curl $PACKER_HTTP_ADDR/$JDK_VERSION.tar.gz | tar xzvf -

# Install Eclipse
ECLIPSE_VERSION=eclipse-java-neon-3-linux-gtk-x86_64
curl $PACKER_HTTP_ADDR/$ECLIPSE_VERSION.tar.gz | tar xzvf -

# The remainder of this script uploads zip files 
# containing additional Eclipse features
# At present these need to be unpacked and installed
# by hand within Eclipse.
/mkdir /opt/eclipse_extras
cd /opt/eclipse_extras

# Subclipse for subversion version control
# https://dl.bintray.com/subclipse/releases/subclipse/subclipse-4.2.2.zip
curl -O $PACKER_HTTP_ADDR/subclipse-4.2.2.zip
sudo apt-get install subversion libsvn-java

# C/C++ development
curl -O $PACKER_HTTP_ADDR/cdt-9.2.1.zip 

# Python development
curl -O $PACKER_HTTP_ADDR/PyDev%205.5.0.zip
curl -O $PACKER_HTTP_ADDR/PyDev%202.8.2.zip
# TODO: Work out which of the downloads above is more useful.

# TODO: Work out the best way of installing all of the above.

# Android development
# Google have deprecated the Eclipse ADT (Android Development Toolkit),
# and the last versions available are now quite hard to find.
# The following link contains the Eclipse plugin only:
# http://dl.google.com/android/ADT-23.0.7.zip
# The following link contains the plugin plus platform-specific
# binaries of the tools it needs:
# http://dl.google.com/android/adt/adt-bundle-linux-x86_64-20140702.zip
curl -O $PACKER_HTTP_ADDR/ADT-23.0.7.zip
# curl -O $PACKER_HTTP_ADDR/adt-bundle-linux-x86_64-20140702.zip
# TODO: Work out which of the downloads above is more useful.

exit 0

# TODO: Stuff below this point is experimental fragments
P2_ARGS="
   -application org.eclipse.equinox.p2.director
   -noSplash
   -profile SDKProfile
   -destination /opt/eclipse
"

# Components available from the eclipse update site
CDT_FEATURES="
   -repository http://$PACKER_HTTP_DIR/cdt-9.2.1.zip
   -installIU org.eclipse.cdt.feature.group  
"
echo /opt/eclipse/eclipse $P2_ARGS $CDT_FEATURES
/opt/eclipse/eclipse $P2_ARGS $CDT_FEATURES

CDT_FEATURES="
   -repository $PACKER_HTTP_DIR/cdt-9.2.1.zip
   -installIU org.eclipse.cdt.feature.group  
"
echo /opt/eclipse/eclipse $P2_ARGS $CDT_FEATURES
/opt/eclipse/eclipse $P2_ARGS $CDT_FEATURES

exit 0




