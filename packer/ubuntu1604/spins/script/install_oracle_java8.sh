#!/bin/sh

# recipe from:
# http://stackoverflow.com/questions/37580900/how-to-install-java8-in-ubuntu-instance-using-packer-provisioner

sudo http_proxy=$http_proxy apt-get update
sudo http_proxy=$http_proxy apt-get install -y software-properties-common

sudo add-apt-repository -y ppa:webupd8team/java
sudo http_proxy=$http_proxy apt-get update
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
sudo http_proxy=$http_proxy apt-get install -y oracle-java8-installer
sudo http_proxy=$http_proxy apt-get install oracle-java8-set-default
