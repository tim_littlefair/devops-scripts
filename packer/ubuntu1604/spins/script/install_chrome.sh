#!/bin/sh

# This script installs a full version of Google's Chrome
# (rather than Ubuntu's build of the open source Chromium 
# repository).

# Get the Debian package 
CHROME_DEB=google-chrome-stable_current_amd64.deb
curl -O $PACKER_HTTP_ADDR/$CHROME_DEB

# Quick but ugly way to install the debian once you have got it:
# dpkg will fail due to missing dependences, but will register 
# a desire to install the chrome .deb into the APT metdadata.
# 'apt-get -y -f install' will see the record of the pending desire,
# and make it happen.
sudo sh -c "if dpkg -i $CHROME_DEB; then echo OK; else apt-get -y -f install; echo OK ; fi"


