#!/bin/sh

# Recipe from:
# http://askubuntu.com/questions/179060/how-not-to-install-recommended-and-suggested-packages

# Note that this script helps to keep the system minimal but 
# may lead to broken dependencies - it puts the onus on other 
# scripts to ensure that essential dependencies are 
# explicitly included

# Default behaviour is for recommends to be installed, suggests to be ignored.
# The created file sets both.

sudo echo '
# Recommends and Suggests are disabled by packer script
# apt_disable_recommends.sh
# If you do not want these disabled, do not run this script.

APT::Install-Recommends "false";
APT::Install-Suggests "false"; 
' > /etc/apt/apt.conf.d/999disable_recommends_and_suggests
