#!/bin/bash

set -e
set -x

# Pick a fast mirror for (optimizing for Perth).

# Iinet's mirror is fast, but has been known to give
# an incomplete set of responses to apt-get update.
sudo sed -i -e 's/us.archive.ubuntu.com\/ubuntu/ftp.iinet.net.au\/pub\/ubuntu/' /etc/apt/sources.list

# Thornlie Christian College provide a mirror 
# (advertised on https://launchpad.net/ubuntu/+archivemirrors) 
# which does not have the same problem, and is nearly as fast 
# as Iinet's. 
#sudo sed -i -e 's/us.archive.ubuntu.com/mirror.tcc.wa.edu.au/' /etc/apt/sources.list

sudo apt-get update
