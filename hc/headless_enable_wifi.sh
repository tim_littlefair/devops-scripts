#! /bin/sh

if [ `whoami` != 'root' ]
then
    echo "This script needs to be run as root to work"
    exit 901
fi

usage() 
{
    echo "Usage: $0 [SSID] [password]"
}

if [ -z "$1" -o -z "$2" ]
then
    usage
    exit 902
fi

ssid=$1
password=$2
ifname=wlp1s0
ipaddr=192.168.1.61


# Recipe for turning on WiFi access based on 
# https://www.linuxbabe.com/ubuntu/connect-to-wi-fi-from-terminal-on-ubuntu-18-04-19-04-with-wpa-supplicant
ws_conf_file=/etc/wpa_supplicant-$ssid.conf
wpa_passphrase "$ssid" "$password" > $ws_conf_file
cat $ws_conf_file

cp -f /lib/systemd/system/wpa_supplicant.service .
sed -e "s#-O /run/wpa_supplicant#-c $ws_conf_file -i $ifname#" -i ./wpa_supplicant.service
sed -e "/Alias=/d" -i ./wpa_supplicant.service
cat ./wpa_supplicant.service
mv -f ./wpa_supplicant.service /etc/systemd/system/

dhc_conf_file=/etc/dhcp/dhclient-$ifname-$ipaddr.conf
cat - > $dhc_conf_file <<+
interface "$ifname" {
     send dhcp-requested-address $ipaddr;
}
+
cat $dhc_conf_file

cat - > ./dhclient.service <<+
[Unit]
Description= DHCP Client
Before=network.target
After=wpa_supplicant.service

[Service]
Type=simple
ExecStart=/sbin/dhclient $ifname -v -cf $dhc_conf_file
ExecStop=/sbin/dhclient $ifname -r
 
[Install]
WantedBy=multi-user.target
+

cat ./dhclient.service
mv -f ./dhclient.service /etc/systemd/system/

systemctl enable wpa_supplicant.service
systemctl enable dhclient.service
systemctl disable NetworkManager-wait-online NetworkManager-dispatcher NetworkManager


exit 0










