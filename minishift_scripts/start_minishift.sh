#!/bin/sh

msdir=minishift-1.21.0-linux-amd64

time sudo --login --user minishift bash -c "cd $msdir ; ./minishift --vm-driver=virtualbox start"
echo Enter ctrl-C to shut minishift down
cat - > /dev/null
sudo --login --user minishift bash -c "cd $msdir ; ./minishift stop"

