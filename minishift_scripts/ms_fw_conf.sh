#!/bin/sh

ipaddr=`sh ./minishift.sh ip`

sudo firewall-cmd --reload
sudo firewall-cmd --add-masquerade
sudo firewall-cmd --zone=public --add-forward-port=port=8443:proto=tcp:toport=8443:toaddr=$ipaddr

