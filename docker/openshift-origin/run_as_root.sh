## Install and verify docker
# apt-get install -y docker.io
# docker run hello-world

## Override default settings to ensure docker uses 
## and insecure registry
sed -e '/DOCKER_OPTS/d' -i /etc/default/docker
cat >> /etc/default/docker <<+
DOCKER_OPTS="--insecure-registry 172.30.0.0/16 --exec-opt native.cgroupdriver=systemd"
+

systemctl daemon-reload
systemctl restart docker


## Create a user to own the OpenSh
# user=openshift1
# useradd -m -G ddocker $user



# Reboot and repeat 'docker run hello-world' to verify that 
# autostart of docker is enabled
