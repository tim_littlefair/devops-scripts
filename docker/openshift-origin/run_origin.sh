#!/bin/sh

echo Running Origin
docker run -d --name "origin" \
        --privileged --pid=host --net=host \
        -v /:/rootfs:ro -v /var/run:/var/run:rw \
        -v /var/lib/docker:/var/lib/docker:rw \
        openshift/origin start

echo Opening a session
docker exec -it origin bash




